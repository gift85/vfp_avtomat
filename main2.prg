set cpdialog off
SET DATE GERMAN
* ���� �� � ������ �������
lip = ''
uch = ''
luser = ''

*this block added for offline mode
PUBLIC offline_test_mode
PUBLIC def_path_to_ink_base && it is for replace path to base
PUBLIC def_path_to_104k_folder && path to 104k/pikuliks folder 
PUBLIC def_path_to_pikuliks_folder && path to alternate folder
offline_test_mode = .f.   && public trigger for offline demo 

IF offline_test_mode == .t.
	def_path_to_ink_base = 'F:\BD_INK_COPY\ink'
	def_path_to_104k_folder = 'F:\BD_INK_COPY\104k_folder\������� �����'
	def_path_to_pikuliks_folder = 'F:\BD_INK_COPY\pikuliks_folder'
ELSE
	def_path_to_ink_base = '\\server2003\u2\ink'
	def_path_to_104k_folder = '\\server2003\u2\104 ���\��� ��������\������� �����'
	def_path_to_pikuliks_folder = '\\server2003\pikulik$'
ENDIF 
*this block added for offline mode

IF !DIRECTORY(def_path_to_ink_base) && was '\\server2003\u2\ink'
	MESSAGEBOX('��� ������� � �������' + CHR(13) + '���������� �������� �� �����' + CHR(13) + '��� �� �� ���.', 64)
	return
ENDIF 
*IF _VFP.StartMode != 0
*	ON ERROR DO errHandler WITH ERROR( ), MESSAGE( ), MESSAGE(1), PROGRAM( ), LINENO( )
*	DO updatechk WITH lip, uch, luser
*ELSE
	ON ERROR
	lip = ''
	uch = '�������'
	luser = 'test'
*ENDIF

DO case
CASE uch == '�������'
	inkpath = def_path_to_ink_base &&'\\5.4.35.3\u2\INK' && '\\5.4.35.3\u2\INK' 'E:\rabota\avtomat' && '\\server2003\u2\INK'
CASE uch == '��'
	inkpath = '\\10.1.55.24\ui\INK'
ENDCASE
IF !DIRECTORY(inkpath)
	MESSAGEBOX('��� ������� � �������' + CHR(13) + '��������� ������� �����������', 64)
	RETURN
ENDIF
set century off
SET DELETED ON 
SET SAFETY OFF 
SET EXCLUSIVE OFF 
SET exact OFF 
SET CONSOLE OFF 
do form main2 WITH lip, inkpath, luser
read events
CLOSE DATABASES 


PROCEDURE errHandler
   PARAMETER merror, mess, mess1, mprog, mlineno
   MESSAGEBOX( '����� ������: ' + LTRIM(STR(merror)) + CHR(13) + ;
    '��������� �� ������: ' + mess + CHR(13) + ;
    '������ ���� � �������: ' + mess1 + CHR(13) + ;
    '����� ������ � �������: ' + LTRIM(STR(mlineno)) + CHR(13) + ;
    '��������� � �������: ' + mprog, 16, '�������� ������ ���������')
   CANCEL 
ENDPROC


FUNCTION forminit(inkpath)
USE inkpath + '\nar\dbf\priem.dbf' ORDER tabn IN 0 SHARED NOUPDATE 
USE inkpath + '\nar\dbf\grnar.dbf' order marsh IN 0 SHARED NOUPDATE
USE inkpath + '\nar\dbf\arms.dbf' order id_arms IN 0 SHARED NOUPDATE
SET RELATION TO tabn INTO priem IN grnar
SET RELATION TO id_akm INTO arms IN priem


FUNCTION scannarday(cur_day, is_cur_day, inkpath)
*cur_day = cur_day - 90

main2.AddObject('MainContainer', 'Container')

M_Container = main2.MainContainer
M_Container.width = 240 * 4 &&+ 20
*M_Container.top = 20
M_Container.zorder(1)
M_Container.visible = .t.
mor_col = 0
eve_col = 0
rbd_col = 0
inner_container_count = 0

DIMENSION rides[3]
slice_day(cur_day, @rides)

main2.label2.caption = main2.label2.caption + CHR(13) + 'm' + STR(rides[1],3) + 'e' + STR(rides[2],3) + 'r' + STR(rides[3],3)

*col_height = INT((rides[1] + rides[2] + rides[3] + 10) / 4) + 1
col_height = INT((rides[1] + rides[2] + rides[3] + 1) / 4) + 1
M_Container.height = (col_height ) * 30 + 15

**���� ����� � ��� ��� ���� � �������� ���� ���������, � �� ���� - ��� ���� ��������� �����
USE 'day_slice.dbf' order marsh
SET RELATION TO tabn INTO priem IN day_slice
SEEK(DTOS(cur_day))

SCAN REST FOR VAL(block) > 9 WHILE datenar = cur_day
	IF day_slice.itmarnam
		new_marsh = .t.
		old_block = day_slice.block
		ink_count = 2
		LOOP
	ENDIF 
	
	IF old_block != day_slice.block
		new_marsh = .t.
		ink_count = 2
	ENDIF 
	
	IF new_marsh
		inner_container_count = inner_container_count + 1
		
		DO case
		CASE VAL(marsh) = 0 && ������� ��� ������
			from_left = int((rides[1] + rides[2] + rbd_col) / col_height) * 240
			from_top = int((rides[1] + rides[2] + rbd_col) % col_height) * 30
			rbd_col = rbd_col + 1
			
			IF rbd_col = 1
				inner_container_count = inner_container_count + 1
				M_Container.AddObject('etc', 'MarshHeader')
				M_Container.etc.caption = '������'
				M_Container.etc.left = from_left  + 40
				M_Container.etc.top = from_top
				M_Container.etc.visible = .t.
				from_left = int((rides[1] + rides[2] + rbd_col) / col_height) * 240
				from_top = int((rides[1] + rides[2] + rbd_col) % col_height) * 30
				rbd_col = rbd_col + 1
			ENDIF 
		CASE VAL(timevih) < 9 && ����� ������ 9�� - ����
			from_left = int((mor_col + rides[2]) / col_height) * 240
			from_top = int((mor_col + rides[2]) % col_height) * 30
			mor_col = mor_col + 1
			
			IF mor_col = 1
				inner_container_count = inner_container_count + 1
				M_Container.AddObject('morn', 'MarshHeader')
				M_Container.morn.caption = '����'
				M_Container.morn.left = from_left + 40
				M_Container.morn.top = from_top
				M_Container.morn.visible = .t.
				from_left = int((mor_col + rides[2]) / col_height) * 240
				from_top = int((mor_col + rides[2]) % col_height) * 30
				mor_col = mor_col + 1
			ENDIF 
		OTHERWISE && ����� - �����
			from_left = int(eve_col / col_height) * 240
			from_top = int(eve_col % col_height) * 30
			eve_col = eve_col + 1	
			
			IF eve_col = 1
				inner_container_count = inner_container_count + 1
				M_Container.AddObject('evening', 'MarshHeader')
				M_Container.evening.caption = '�����'
				M_Container.evening.left = from_left + 40
				M_Container.evening.top = from_top
				M_Container.evening.visible = .t.
				from_left = int(eve_col / col_height) * 240
				from_top = int(eve_col % col_height) * 30
				eve_col = eve_col + 1
			ENDIF 	
		ENDCASE  
	
		addMarshContainer(ALLTRIM(day_slice.marsh), inner_container_count, from_top, from_left)
		
		ink_count = ink_count + 1
		addInkToMarsh(inner_container_count, ink_count, day_slice.tabn)
		
		new_marsh = .f.
		old_block = day_slice.block
	ELSE 
		ink_count = ink_count + 1
		addInkToMarsh(inner_container_count, ink_count, day_slice.tabn)
		IF (M_Container.controls(inner_container_count).controlcount - 2) % 6 = 0
			M_Container.controls(inner_container_count).height = ;
			 M_Container.controls(inner_container_count).height + 30
			DO CASE
			CASE VAL(marsh) = 0
				rbd_col = rbd_col + 1
			CASE VAL(timevih) < 8
				mor_col = mor_col + 1
			OTHERWISE
				eve_col = eve_col + 1
			ENDCASE
			
			IF M_Container.controls(inner_container_count).height + ;
			  M_Container.controls(inner_container_count).top > M_Container.height
				M_Container.height = M_Container.height + 30
			ENDIF 
		ENDIF 	
	ENDIF 
	
ENDSCAN  
USE 



FUNCTION addMarshContainer(mrsh, num, from_top, from_left)
LOCAL l
mnum = padl(ALLTRIM(mrsh), 3, '0')
*mnum = CPCONVERT(1251, 866, mnum) && ������������� ��� ������
mnum = STRTRAN(mnum, '-')
container_name = 'm_' + mnum 
check_name = 'check' + mnum 
label_name = 'l_' + mnum 

main2.MainContainer.AddObject(container_name, 'MarshContainer')  && ��������� ��������
c_control = main2.MainContainer.controls(num)
c_control.left = from_left
c_control.top = from_top 
c_control.visible = .t.
c_control.AddObject(check_name, 'MarshCheck')
c_control.controls(1).visible = .t.

c_control.AddObject(label_name, 'MarshLabel')
c_control.controls(2).caption = mnum
c_control.controls(2).visible = .t.


FUNCTION addInkToMarsh(n, i, tabn)
c_control = main2.MainContainer.controls(n)
i = c_control.controlcount + 1

label_name = 'ink' + padl(ALLTRIM(STR(i)), 3, '0') + 'ak' + PADL(ALLTRIM(arms.storage), 3, '0')
c_control.AddObject('L' + label_name, 'Inkassator') 

c_control.controls(i).left = 80 * (((i - 1) / 2) % 3)
c_control.controls(i).top = 30 * INT(((i - 1) / 2) / 3)
c_control.controls(i).width = 80
c_control.controls(i).visible = .t.
c_control.controls(i).aks_num = PADL(ALLTRIM(arms.storage), 3, '0')
c_control.controls(i).caption = tabn + ' ' + priem.brifio + CHR(13) + ;
							' ' + PADL(ALLTRIM(arms.storage), 3, '0') + ' ' + STR(avtom)
c_control.controls(i).tooltiptext = tabn + ' ' + priem.brifio + CHR(13) + ;
							' ' + PADL(ALLTRIM(arms.storage), 3, '0') + ' ' + STR(avtom)
							
c_control.AddObject('W' + label_name, 'WCheck')
c_control.controls(i + 1).left = 80 * (((i - 1) / 2) % 3)
c_control.controls(i + 1).top = 30 * INT(((i - 1) / 2) / 3) + 15
IF avtom != 1
*!*		c_control.controls(i).fontbold = .t.
*!*		c_control.controls(i).backcolor = RGB(225, 225, 0)
*!*		c_control.controls(i).defcolor = RGB(225, 225, 0)
*!*		c_control.controls(i + 1).fontbold = .t.
*!*		c_control.controls(i + 1).backcolor = RGB(225, 225, 0)
*!*		c_control.controls(i + 1).defcolor = RGB(225, 225, 0)
*!*	ELSE
	c_control.controls(i + 1).value = 0
	*switch_weapon_label(c_control.controls(i + 1))
ENDIF 
switch_weapon_label(c_control.controls(i + 1))
c_control.controls(i + 1).visible = .t.


FUNCTION setInkColor(aknum, fixedcolor, freecolor)
aknum = STRTRAN(aknum, LEFT(aknum, 7))
FOR i = 1 TO main2.MainContainer.controlcount
	IF main2.MainContainer.controls(i).class != 'Marshcontainer'
		LOOP
	ENDIF 
	FOR k = 1 TO main2.MainContainer.controls(i).controlcount
		m_control = main2.MainContainer.controls(i).controls(k)
		
		IF ATC(aknum, m_control.name) != 0
			DO CASE 
			CASE EMPTY(fixedcolor)
				m_control.backcolor = m_control.defcolor
			CASE m_control.fontbold 
				m_control.backcolor = fixedcolor
			OTHERWISE 
				m_control.backcolor = freecolor
			ENDCASE 
		ENDIF
	ENDFOR 
ENDFOR 


FUNCTION switch_weapon_label(obj)
IF obj.value = 1
	obj.caption = '���'
	obj.aks_fixed = .t.
	obj.parent.controls(obj.tabindex - 1).aks_fixed = .t.
	obj.fontbold = .t.
	obj.parent.controls(obj.tabindex - 1).fontbold = .t.
	obj.backcolor = RGB(225, 225, 0)
	obj.parent.controls(obj.tabindex - 1).backcolor = RGB(225, 225, 0)
	obj.defcolor = RGB(225, 225, 0)
	obj.parent.controls(obj.tabindex - 1).defcolor = RGB(225, 225, 0)
ELSE
	obj.caption = '��'
	obj.aks_fixed = .f.
	obj.parent.controls(obj.tabindex - 1).aks_fixed = .f.
	obj.fontbold = .f.
	obj.parent.controls(obj.tabindex - 1).fontbold = .f.
	obj.backcolor = RGB(240, 240, 240)
	obj.parent.controls(obj.tabindex - 1).backcolor = RGB(240, 240, 240)
	obj.defcolor = RGB(240, 240, 240)
	obj.parent.controls(obj.tabindex - 1).defcolor = RGB(240, 240, 240)
ENDIF 


FUNCTION slice_day(cur_day, rides)
cell_len = 3
STORE 0 TO rides[1]
STORE 0 TO rides[2]
STORE 0 TO rides[3]

SELECT grnar
SEEK(DTOS(cur_day))

COPY TO 'day_slice.dbf' REST FOR VAL(block) > 9 WHILE datenar = cur_day WITH cdx as 1251
SELECT 0

*���������� ��������� ��� ������(������)
SELECT marsh, COUNT(tabn) ;
FROM 'day_slice.dbf' ;
WHERE VAL(block) > 9 and datenar = cur_day AND VAL(marsh) = 0 AND itmarnam = .f.;
group BY marsh ;
INTO table 'rbd.dbf'
SCAN 
	rides[3] = rides[3] + CEILING((cnt_tabn + 1) / cell_len)
ENDSCAN 
USE

*���������� �������� ���������
SELECT marsh, COUNT(tabn) ;
FROM 'day_slice.dbf' ;
WHERE VAL(block) > 9 and datenar = cur_day AND VAL(marsh) != 0 AND VAL(timevih) < 9 AND itmarnam = .f.;
group BY marsh ;
INTO table 'morning.dbf'
SCAN 
	rides[1] = rides[1] + CEILING((cnt_tabn + 1) / cell_len)
ENDSCAN 
USE

*���������� �������� ���������
SELECT marsh, COUNT(tabn) ;
FROM 'day_slice.dbf' ;
WHERE VAL(block) > 9 and datenar = cur_day AND VAL(marsh) != 0 AND VAL(timevih) >= 9 AND itmarnam = .f.;
group BY marsh ;
INTO table 'evening.dbf'
SCAN 
	rides[2] = rides[2] + CEILING((cnt_tabn + 1) / cell_len)
ENDSCAN 
USE

SELECT day_slice
USE 

main2.label2.caption = 'm' + STR(rides[1], 3) + 'e' + STR(rides[2], 3) + 'r' + STR(rides[3], 3)

rides[1] = IIF(rides[1] > 0, rides[1] + 1, 0)
rides[2] = IIF(rides[2] > 0, rides[2] + 1, 0)
rides[3] = IIF(rides[3] > 0, rides[3] + 1, 0)

main2.label2.caption = main2.label2.caption + CHR(13) + ;
     'm' + STR(rides[1], 3) + 'e' + STR(rides[2], 3) + 'r' + STR(rides[3], 3)

return rides



*============classes
DEFINE CLASS MarshContainer AS Container  && ����� ���������� ��� ���������
   Left = 0  
   Top = 0  
   Height = 30
   width = 240 
ENDDEFINE

DEFINE CLASS Inkassator AS Label  && ����� ���������� � ����������� �� ��������
	caption = 'label'
    Left = 0  
    Top = 0  
    Height = 30
    width = 80 
    borderstyle = 1
    defcolor = rgb(240, 240, 240)
    aks_num = '000'
    aks_fixed = .f.
    
    PROCEDURE Click
	   IF this.parent.controls(this.tabindex + 1).value = 0
	       this.parent.controls(this.tabindex + 1).value = 1
	   ELSE
	       this.parent.controls(this.tabindex + 1).value = 0
	   ENDIF
       this.parent.controls(this.tabindex + 1).Click
       main2.label2.caption = main2.MainContainer.controls(3).class

    PROCEDURE MouseEnter(nButton, nShift, nXCoord, nYCoord)
   		setInkColor(this.name, rgb(255, 0, 0), rgb(0, 255, 0))
   		
   	PROCEDURE MouseLeave(nButton, nShift, nXCoord, nYCoord)
   		setInkColor(this.name)
ENDDEFINE

DEFINE CLASS MarshLabel AS Label  && ����� �������� ��������
    Left = 0  
    Top = 0  
    Height = 30
    width = 50 
    borderstyle = 1
    fontsize = 19
    backcolor = RGB(0, 0, 0)
    forecolor = RGB(240, 240, 240)
ENDDEFINE

DEFINE CLASS MarshHeader AS MarshLabel  && ����� "����"\"�����"\"������"
    width = 160 
    alignment = 2
ENDDEFINE

DEFINE CLASS MarshCheck AS Checkbox  && ����� �������� "�" 
	caption = '�'
	value = 1
    Left = 51
    Top = 5 
    Height = 20
    width = 28 
ENDDEFINE

DEFINE CLASS WCheck AS Checkbox  && ����� �������� ������ 
	caption = '���'
	value = 1
    Left = 0
    Top = 15
    Height = 14
    width = 79
    alignment = 2
    defcolor = rgb(240, 240, 240)
    aks_num = '000'
    aks_fixed = .f.
   
    PROCEDURE Click
        switch_weapon_label(this)
   
    PROCEDURE MouseEnter(nButton, nShift, nXCoord, nYCoord)
   		setInkColor(this.name, rgb(255, 0, 0), rgb(0, 255, 0))
   		
   	PROCEDURE MouseLeave(nButton, nShift, nXCoord, nYCoord)
   		setInkColor(this.name)
ENDDEFINE