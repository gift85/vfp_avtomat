set cpdialog off
SET DATE GERMAN
* ���� �� � ������ �������
lip = ''
uch = ''
luser = ''

*this block added for offline mode
PUBLIC offline_test_mode
PUBLIC def_path_to_ink_base && it is for replace path to base
PUBLIC def_path_to_104k_folder && path to 104k/pikuliks folder 
PUBLIC def_path_to_pikuliks_folder && path to alternate folder
offline_test_mode = .f.   && public trigger for offline demo 

IF offline_test_mode == .t.
	def_path_to_ink_base = 'F:\BD_INK_COPY_161028\ink'
	def_path_to_104k_folder = 'F:\BD_INK_COPY_161028\104k_folder\������� �����'
	def_path_to_pikuliks_folder = 'F:\BD_INK_COPY_161028\pikuliks_folder'
ELSE
	def_path_to_ink_base = '\\server2003\u2\ink'
	def_path_to_104k_folder = '\\server2003\u2\104 ���\��� ��������\������� �����'
	def_path_to_pikuliks_folder = '\\server2003\pikulik$'
ENDIF 
*this block added for offline mode

IF !DIRECTORY(def_path_to_ink_base) && was '\\server2003\u2\ink'
	MESSAGEBOX('��� ������� � �������' + CHR(13) + '���������� �������� �� �����' + CHR(13) + '��� �� �� ���.', 64)
	return
ENDIF 
IF _VFP.StartMode != 0
	ON ERROR DO errHandler WITH ERROR( ), MESSAGE( ), MESSAGE(1), PROGRAM( ), LINENO( )
	DO updatechk WITH lip, uch, luser
ELSE
	ON ERROR
	lip = ''
	uch = '�������'
	luser = 'test'
ENDIF

DO case
CASE uch == '�������'
	MESSAGEBOX('���������: ��������� ����� ������ ���������������� ��������!' + CHR(13) + ;
	 '���� ����� ����� ��������� ����� �� ���' + CHR(13) + ;
	 '������ ��������� ����� ��������������.', 64)
	*RETURN 
	inkpath = def_path_to_ink_base &&'\\5.4.35.3\u2\INK' && '\\5.4.35.3\u2\INK' 'E:\rabota\avtomat' && '\\server2003\u2\INK'
CASE uch == '��'
	MESSAGEBOX('���������: ��������� ����� ������ ���������������� ��������!' + CHR(13) + ;
	 '�� ������� ��������� ��� ��������� ��-�� ��������� ������������ ����� �� ���!' + CHR(13) + ;
	 '������ ��������� ����� ���� �������������� � ������ ����� �� ��������.', 64)
	inkpath = '\\10.1.55.24\ui\INK'
ENDCASE
IF !DIRECTORY(inkpath)
	MESSAGEBOX('��� ������� � �������' + CHR(13) + '��������� ������� �����������', 64)
	RETURN
ENDIF
set century off
SET DELETED ON 
SET SAFETY OFF 
SET EXCLUSIVE OFF 
SET exact OFF 
SET CONSOLE OFF 
do form main WITH lip, inkpath, luser
read events
CLOSE DATABASES 

PROCEDURE errHandler
   PARAMETER merror, mess, mess1, mprog, mlineno
   MESSAGEBOX( '����� ������: ' + LTRIM(STR(merror)) + CHR(13) + ;
    '��������� �� ������: ' + mess + CHR(13) + ;
    '������ ���� � �������: ' + mess1 + CHR(13) + ;
    '����� ������ � �������: ' + LTRIM(STR(mlineno)) + CHR(13) + ;
    '��������� � �������: ' + mprog, 16, '�������� ������ ���������')
   CANCEL 
ENDPROC

FUNCTION TEKDAYINIT
LPARAMETERS tekday, inkpath
IF USED('tekday')
	SELECT tekday 
	USE
ENDIF 
IF FILE('tekday.dbf')
	DELETE FILE 'tekday.dbf'
ENDIF 
SELECT 0
CREATE TABLE 'tekday.dbf' ;
 (datenar d, marsh c(3), tabn c(4), brifio c(16), stsb c(1), timevih c(5), avtom n(1), id_akm n(7), zmt c(3), zmts c(3), zmtv c(3), ;
  akm_st n(1), notchk n(2), num_cont n(3), pos n(1), lstdrab d, regrab c(42))
USE inkpath + '\nar\dbf\priem.dbf' ORDER tabn IN 0 SHARED NOUPDATE 
USE inkpath + '\nar\dbf\grnar.dbf' order marsh IN 0 SHARED NOUPDATE 
SELECT grnar
SET RELATION TO tabn INTO priem
SEEK DTOS(tekday)
IF FOUND()
	SCAN REST FOR !itmarnam AND VAL(block) > 9 WHILE datenar = tekday
		INSERT INTO 'tekday.dbf' (datenar,       marsh,       tabn,       brifio,       stsb,       timevih,       avtom, ;
		                                                 id_akm,       zmt,         zmts,         zmtv,         lstdrab,       regrab);
		                  values (grnar.datenar, grnar.marsh, grnar.tabn, priem.brifio, grnar.stsb, grnar.timevih, grnar.avtom, ;
		                                                 priem.id_akm, priem.marsh, priem.marshs, priem.marshv, priem.lstdrab, priem.regrab)
	ENDSCAN 
	nullday = .f.
ELSE
	nullday = .t.
ENDIF 
SELECT priem
USE
SELECT grnar
USE 
RETURN nullday

FUNCTION scannarday
LPARAMETERS tekday, istekday, inkpath
main.label1.caption = ''
main.label1.fontsize = 9
main.command2.Enabled = .F.
main.container63.setall('tooltiptext', '')
main.container63.setall('fontbold', .f., 'Label')
main.container63.setall('backcolor', RGB(240, 240, 240), 'Label')
main.container63.setall('caption', '')
main.container63.setall('value', 0, 'Checkbox')
main.container63.setall('forecolor', RGB(0, 0, 0))
main.container63.setall('fontunderline', .f.)
main.container63.setall('alignment', 2, 'Label')
IF istekday
	IF tekdayinit(tekday, inkpath)
		main.label1.caption = main.label1.caption + '������ ���!' + CHR(13)
		main.command1.enabled = .f.
		RETURN 
	ENDIF 
	main.edit1.value = '������ �������:' + chr(13)
	main.edit2.value = '� ���� ����� �������:' + chr(13)
	main.label1.backcolor = RGB(240, 240, 240)
	SELECT tekday 
ENDIF	 
GO TOP 
allmt = 0
mt = 0
mt90 = 0
mt91 = 0
mtrb = 0
mtsr = ''
SCAN
	DO CASE 
	CASE VAL(tekday.marsh) = 90 OR VAL(tekday.marsh) = 142
		IF mt90 = 0
			check1_caption = tekday.marsh
			check1_enabled = .t.
		ENDIF
		mt90 = mt90 + 1
		rep_num_cont = 77 &&62
		rep_pos = mt90
		check1_value = 1
	CASE VAL(tekday.marsh) = 91
		IF mt91 = 0
			check1_caption = tekday.marsh
			check1_enabled = .t.
		ENDIF
		mt91 = mt91 + 1
		rep_num_cont = 76 &&61
		rep_pos = mt91
		check1_value = 1
	CASE VAL(tekday.marsh) = 0
		check1_caption = tekday.marsh
		check1_enabled = .t.
		allmt=allmt + 1
		rep_num_cont = allmt
		rep_pos = 1
		check1_value = 0
	OTHERWISE
		IF mtsr != RTRIM(tekday.marsh)
			IF mt = 1
				main.label1.caption = main.label1.caption + '�-� ' + mtsr + ' �� ������!' + CHR(13)
				main.command1.enabled = .t.
			ENDIF 
			allmt = allmt + 1
			rep_num_cont = allmt
			check1_caption = tekday.marsh
			check1_enabled = .t.
			check1_value = 1 
			mt = 0
		ENDIF 
		mt = mt + 1
		rep_pos = mt
		mtsr = RTRIM(tekday.marsh)
	ENDCASE 	
	IF rep_pos = 1
		main.container63.controls(rep_num_cont).check1.caption = check1_caption
		main.container63.controls(rep_num_cont).check1.tooltiptext = '����� ������ ' + tekday.timevih
		main.container63.controls(rep_num_cont).check1.value = check1_value
		main.container63.controls(rep_num_cont).check1.enabled = check1_enabled
	ENDIF
	main.container63.controls(rep_num_cont).controls(rep_pos + 1).caption = ALLTRIM(tekday.tabn) + CHR(13) + ALLTRIM(tekday.brifio)
	replace tekday.num_cont WITH rep_num_cont
	replace tekday.pos WITH rep_pos
	DO CASE 
	CASE tekday.avtom = 1
		main.container63.controls(rep_num_cont).controls(rep_pos + 1).backcolor = RGB(240, 240, 0)
	OTHERWISE 
		main.container63.controls(rep_num_cont).controls(rep_pos + 1).backcolor = RGB(240, 240, 240)
	ENDCASE 
	IF VAL(tekday.stsb) = 0
		main.container63.controls(rep_num_cont).controls(rep_pos + 1).fontbold = .t.
	ELSE
		main.container63.controls(rep_num_cont).controls(rep_pos + 1).fontbold = .f.
	ENDIF
	IF !INLIST(VAL(tekday.marsh), VAL(tekday.zmt), VAL(tekday.zmts), VAL(tekday.zmtv))
		main.container63.controls(rep_num_cont).controls(rep_pos + 1).forecolor = RGB(180, 0, 0)
	ELSE 
		IF vihnotvih(tekday, tekday.lstdrab, ALLTRIM(tekday.regrab), .f., .t., tekday.stsb)
			DO case
			CASE vihnotvih(tekday, tekday.lstdrab, ALLTRIM(tekday.regrab), .t., .f., '3')
				main.container63.controls(rep_num_cont).controls(rep_pos + 1).fontunderline = .t.
			case tekday.stsb = '0'
				main.container63.controls(rep_num_cont).controls(rep_pos + 1).forecolor = RGB(0, 150, 0)
			CASE tekday.stsb = '1'
				main.container63.controls(rep_num_cont).controls(rep_pos + 1).forecolor = RGB(0, 0, 255)
			ENDCASE
		ENDIF 
	ENDIF 
	lkagain(tekday.id_akm, RTRIM(tekday.tabn), RTRIM(tekday.marsh), rep_num_cont, rep_pos, RECNO())
ENDSCAN 
IF ISTEKDAY
	USE 
ENDIF 
*!*	IF allmt = 0
*!*		main.label1.caption = main.label1.caption + '������ ���!' + CHR(13)
*!*		main.command1.enabled = .f.
*!*	else
*!*		IF mt90 != 5 AND mt90 > 0
*!*			main.label1.caption = main.label1.caption + '�-� 90 �� ������!' + CHR(13)
*!*		ENDIF
*!*		IF mt91 != 2 AND mt91 > 0
*!*			main.label1.caption = main.label1.caption + '�-� 91 �� ������!' + CHR(13)
*!*		ENDIF
	main.command1.enabled=.t.
*ENDIF 

FUNCTION lkagain
LPARAMETERS navt, tabnum, mar, contnum, lblnum, nrec, repl_akmst
main.container63.controls(contnum).controls(lblnum + 1).tooltiptext=  '' && lbl=''
IF navt = 0
	main.container63.controls(contnum).controls(lblnum + 1).tooltiptext = '� ' + RTRIM(tekday.brifio) + ' ���� �������� �� :)' &&lbl
	main.container63.controls(contnum).controls(lblnum + 1).backcolor = RGB(240, 240, 240) &&lbl3
	replace tekday.akm_st WITH 0
	return
ENDIF 
nfound = .t.
&&SELECT 1&&2
GO TOP 
LOCATE FOR navt = tekday.id_akm
DO WHILE FOUND()
	IF !EMPTY(repl_akmst)
		replace tekday.akm_st WITH repl_akmst
	ENDIF 
	IF tabnum = rTRIM(tekday.tabn) AND mar = RTRIM(tekday.marsh) && AND VAL(mar)!=0
		IF tekday.avtom = 1 OR tekday.avtom = 3
			nfound = .f.
			main.container63.controls(contnum).controls(lblnum + 1).tooltiptext = '��� ��������� �� ���� ��������!' + CHR(10) + ;
			 main.container63.controls(contnum).controls(lblnum + 1).tooltiptext
			IF VAL(tekday.marsh) != 0
				replace tekday.akm_st WITH 2
			ELSE 
				replace tekday.akm_st WITH 1
			ENDIF 
			IF tekday.avtom = 3
				main.container63.controls(contnum).controls(lblnum + 1).backcolor = RGB(128, 128, 255)
			endif
		ELSE
			*&lbl = &lbl2 + '�� ���� �������� ' + RTRIM(fio) + CHR(10)
		ENDIF 
	ELSE
		IF tekday.avtom = 1 OR tekday.avtom = 3
			nfound = .f.
			main.container63.controls(contnum).controls(lblnum + 1).tooltiptext = '��� ��������� �� ' + RTRIM(tekday.marsh) + ' ��������: � ' + RTRIM(tekday.brifio) + CHR(10) + ;
			 main.container63.controls(contnum).controls(lblnum + 1).tooltiptext
			IF tekday.avtom = 1
				main.container63.controls(contnum).controls(lblnum + 1).backcolor = RGB(220, 128, 128)
			endif
			replace tekday.akm_st WITH 2
		ELSE
			main.container63.controls(contnum).controls(lblnum + 1).tooltiptext = main.container63.controls(contnum).controls(lblnum + 1).tooltiptext + ;
			 '���� ��� ��� � ' + RTRIM(tekday.brifio) + ', �� �� ' + RTRIM(tekday.marsh) + ' �-�� ��� ����.' + CHR(10)
		ENDIF 
	ENDIF
	CONTINUE 
ENDDO
IF nfound
	main.container63.controls(contnum).controls(lblnum + 1).tooltiptext = '��� ��������!' + CHR(10) + ;
	 main.container63.controls(contnum).controls(lblnum + 1).tooltiptext
	main.container63.controls(contnum).controls(lblnum + 1).backcolor = RGB(0, 200, 0)
	GO nrec 
	replace tekday.akm_st with 3
ENDIF 
*0-��� �������� 1- ���������  � �������\��� 2 - ��������� �� ���� 3 - ��������
GO nrec
main.container63.controls(contnum).controls(lblnum + 1).tooltiptext = ;
 RTRIM(tekday.marsh) + IIF(VAL(tekday.marsh) = 0, ' ', ' �-� ') + ;
 IIF(tekday.stsb = '0', ',������� - ', IIF(tekday.stsb = '1', ',������� - ', '- ')) + RTRIM(tekday.brifio) + CHR(10) + ;
    main.container63.controls(contnum).controls(lblnum + 1).tooltiptext
*SELECT 1

FUNCTION tryaks
LPARAMETERS secondtry
main.edit1.value = '������ �������:' + chr(13)
main.edit2.value = '� ���� ����� �������:' + chr(13)
main.label1.backcolor = RGB(240, 240, 240)
scaning = .t.
counts = 0
vseok = .t.
IF secondtry = 1
	COPY FILE 'tekday.dbf' TO 'tekday1.dbf'
ELSE 
	COPY FILE 'tekday1.dbf' TO 'tekday.dbf'
ENDIF 
USE 'tekday.dbf' && IN 1&&3
DO WHILE scaning and counts < 1000
	&&SELECT 1&&3
	counts = counts + 1
	GO top
	LOCATE FOR VAL(tekday.stsb) = 0 AND tekday.avtom = 0 AND VAL(tekday.marsh) > 0 && AND notchk < 20 && and !inlist(val(marsh), 90, 91)
	IF FOUND()
		vseok = .f.
		*lbl = 'main.container' + ALLTRIM(STR(num_cont, 3)) + '.check1.value'
		do case
		*CASE &lbl = 0
		*	replace notchk WITH 20
		CASE tekday.akm_st = 3
			replace tekday.avtom WITH 1
			Sakm_id2 = tekday.id_akm
			GO TOP 
			LOCATE FOR Sakm_id2 = tekday.id_akm AND tekday.akm_st > 2
			DO WHILE FOUND()
				replace tekday.akm_st WITH 2
				CONTINUE 
			ENDDO 
		OTHERWISE  
			*replace notchk WITH 10
			Sstsb = ALLTRIM(tekday.stsb)
			Savtom = tekday.avtom
			Sakm_st = tekday.akm_st
			Sakm_id = tekday.id_akm
			Snum_cont = tekday.num_cont
			Spos = tekday.pos
			Stabn = padr(tekday.tabn, 5)
			Sreprec = RECNO()
			Sbrifio = ALLTRIM(tekday.brifio)
			Smarsh = ALLTRIM(tekday.marsh)
			GO top
			LOCATE FOR VAL(tekday.stsb) = 1 AND tekday.num_cont = Snum_cont
			IF FOUND()
				IF tekday.akm_st = 3 &&���� ������� � �������� ��������
					Sstsb2 = ALLTRIM(tekday.stsb)
					replace tekday.stsb WITH Sstsb
					replace tekday.avtom WITH 1
					Sakm_id2 = tekday.id_akm
					IF AT(padr(tekday.tabn, 5) + ALLTRIM(tekday.brifio) + ' <<' + ALLTRIM(tekday.marsh) + '>> ' + Stabn + Sbrifio + CHR(13), main.edit1.value) = 0
						main.edit1.value = main.edit1.value + Stabn + Sbrifio + ' <<' + Smarsh + '>> ' + padr(tekday.tabn, 5) + ALLTRIM(tekday.brifio) + CHR(13)
					ELSE
						main.edit1.value = STRTRAN(main.edit1.value,padr(tekday.tabn, 5) + ALLTRIM(tekday.brifio) + ' <<' + ALLTRIM(tekday.marsh) + '>> ' + Stabn + Sbrifio + CHR(13))
						main.edit2.value = main.edit2.value + '����� ������: ' + Stabn + Sbrifio + ' (' + Smarsh + ')' + ' � ' + padr(tekday.tabn, 5) + ALLTRIM(tekday.brifio) + ' (' + ALLTRIM(tekday.marsh) + ')' + CHR(13)
					ENDIF 
					GO TOP 
					LOCATE FOR Sakm_id2 = tekday.id_akm AND tekday.akm_st > 2 && and !inlist(val(marsh), 90, 91)
					DO WHILE FOUND()
						replace tekday.akm_st WITH 2
						CONTINUE 
					ENDDO 
					GO Sreprec
					replace tekday.stsb WITH Sstsb2
					*scannarday(main.text1.value, .f., main.comment)
				ELSE 
					GO top
					LOCATE FOR tekday.id_akm = Sakm_id AND tekday.num_cont != Snum_cont AND tekday.avtom = 1 AND VAL(tekday.stsb) != 1 && AND tekday.notchk < 20 && and !inlist(val(marsh), 90, 91)
					IF FOUND()
						IF inlist(val(tekday.marsh), 90, 91)
							replace tekday.avtom WITH 3
						ELSE 
							replace tekday.avtom WITH 0 
							IF VAL(tekday.stsb) = 0
								replace tekday.stsb WITH '1'
							ENDIF
							Stabn2 = padr(tekday.tabn, 5)
							Snum_cont2 = tekday.num_cont
							Sbrifio2 = ALLTRIM(tekday.brifio)
							Smarsh2 = ALLTRIM(tekday.marsh)
							replace tekday.notchk WITH 20 &&
							main.edit2.value = main.edit2.value + '������ � ' + ALLTRIM(tekday.brifio) + ' (' + ALLTRIM(tekday.marsh) + '), ��� ' + Sbrifio + ' (' + Smarsh + ')' + CHR(13)
							GO top
							LOCATE FOR Stabn2 != padr(tekday.tabn, 5) AND tekday.num_cont = Snum_cont2
							IF FOUND()
								replace tekday.stsb WITH '0'
								IF AT(padr(tekday.tabn, 5) + ALLTRIM(tekday.brifio) + ' <<' + ALLTRIM(tekday.marsh) + '>> ' + Stabn2 + Sbrifio2 + CHR(13), main.edit1.value) = 0
									main.edit1.value = main.edit1.value + Stabn2 + Sbrifio2 + ' <<' + Smarsh2 + '>> ' + padr(tekday.tabn, 5) + ALLTRIM(tekday.brifio) + CHR(13)
								ELSE
									main.edit1.value = STRTRAN(main.edit1.value, padr(tekday.tabn, 5) + ALLTRIM(tekday.brifio) + ' <<' + ALLTRIM(tekday.marsh) + '>> ' + Stabn2 + Sbrifio2 + CHR(13))
									main.edit2.value = main.edit2.value + '����� ������: ' + Sbrifio2 + ' (' + Smarsh2 + ') � ' + ALLTRIM(tekday.brifio) + ' (' + ALLTRIM(tekday.marsh) + ')' + CHR(13)
								ENDIF
							ENDIF 
						ENDIF
						GO Sreprec
						replace tekday.avtom WITH 1
						*scannarday(main.text1.value, .f., main.comment)
					ENDIF 				
				ENDIF 
			ELSE 
				*GO Sreprec
				*replace notchk WITH 20 &&
				*WAIT WINDOW marsh + ' - �� �������������!' TIMEOUT 5 NOWAIT 
					GO top&&
					LOCATE FOR tekday.id_akm = Sakm_id AND tekday.num_cont != Snum_cont AND tekday.avtom = 1 AND VAL(tekday.stsb) != 1 && AND tekday.notchk < 20 && and !inlist(val(marsh), 90, 91)
					IF FOUND()
						IF inlist(val(tekday.marsh), 90, 91)
							replace tekday.avtom WITH 3
						ELSE 
							replace tekday.avtom WITH 0 
							IF VAL(tekday.stsb) = 0
								replace tekday.stsb WITH '1'
							ENDIF
							Stabn2 = padr(tekday.tabn, 5)
							Snum_cont2 = tekday.num_cont
							Sbrifio2 = ALLTRIM(tekday.brifio)
							Smarsh2 = ALLTRIM(tekday.marsh)
							replace tekday.notchk WITH 20 &&
							main.edit2.value = main.edit2.value + '������ � ' + ALLTRIM(tekday.brifio) + ' (' + ALLTRIM(tekday.marsh) + '), ��� ' + Sbrifio + ' (' + Smarsh + ')' + CHR(13)
							GO top
							LOCATE FOR Stabn2 != padr(tekday.tabn, 5) AND tekday.num_cont = Snum_cont2
							IF FOUND()
								replace tekday.stsb WITH '0'
								IF AT(padr(tekday.tabn, 5) + ALLTRIM(tekday.brifio) + ' <<' + ALLTRIM(tekday.marsh) + '>> ' + Stabn2 + Sbrifio2 + CHR(13), main.edit1.value) = 0
									main.edit1.value = main.edit1.value + Stabn2 + Sbrifio2 + ' <<' + Smarsh2 + '>> ' + padr(tekday.tabn, 5) + ALLTRIM(tekday.brifio) + CHR(13)
								ELSE
									main.edit1.value = STRTRAN(main.edit1.value, padr(tekday.tabn, 5) + ALLTRIM(tekday.brifio) + ' <<' + ALLTRIM(tekday.marsh) + '>> ' + Stabn2 + Sbrifio2 + CHR(13))
									main.edit2.value = main.edit2.value + '����� ������: ' + Sbrifio2 + ' (' + Smarsh2 + ') � ' + ALLTRIM(tekday.brifio) + ' (' + ALLTRIM(tekday.marsh) + ')' + CHR(13)
								ENDIF
							ENDIF 
						ENDIF
						GO Sreprec
						replace tekday.avtom WITH 1
						*scannarday(main.text1.value, .f., main.comment)
					ENDIF &&			 
			ENDIF 
		ENDCASE  
	ELSE
		scaning = .f.
		WAIT WINDOW 'Well done!' TIMEOUT 5 NOWAIT 
		main.label1.backcolor = RGB(0, 255, 0)
	ENDIF 
ENDDO 
GO top
FOR il = 1 TO 3
scan FOR !INLIST(VAL(tekday.marsh), VAL(tekday.zmt), VAL(tekday.zmts), VAL(tekday.zmtv)) AND tekday.akm_st = 3 AND tekday.avtom = 0 AND VAL(tekday.stsb) = 1
	Vnumcont = tekday.num_cont
	Vtabn    = tekday.tabn
	Vmarsh   = ALLTRIM(tekday.marsh)
	Vbrifio  = ALLTRIM(tekday.brifio)
	VnRec    = RECNO()
	GO TOP 
	LOCATE FOR Vnumcont = tekday.num_cont AND tekday.stsb = '0' AND vtabn != tekday.tabn AND tekday.avtom = 1 AND tekday.stsb = '0' AND INLIST(VAL(tekday.marsh), VAL(tekday.zmt), VAL(tekday.zmts), VAL(tekday.zmtv))
	IF FOUND()
		Vakm_st = tekday.akm_st
		Vstsb   = tekday.stsb
		Vavtom  = tekday.avtom
		replace tekday.akm_st WITH 3
		replace tekday.stsb WITH '1'
		replace tekday.avtom WITH 0
		main.edit1.value = main.edit1.value + PADr(Vtabn, 5) + Vbrifio + ' (��� �-�) <<' + Vmarsh + '>> ' + padr(tekday.tabn, 5) + ALLTRIM(tekday.brifio) + CHR(13)
		GO VnRec
		replace tekday.akm_st WITH Vakm_st
		replace tekday.stsb WITH Vstsb
		replace tekday.avtom WITH Vavtom	
		lkagain(tekday.id_akm, RTRIM(tekday.tabn), RTRIM(tekday.marsh), tekday.num_cont, tekday.pos, RECNO(), Vakm_st)
	ELSE
		GO VnRec
		replace tekday.avtom WITH 4
	ENDIF 
	GO TOP 
ENDSCAN 
scannarday(main.text1.value, .f., main.comment)
ENDFOR 
*SELECT 1
USE
*CLOSE DATABASES 
IF vseok OR EMPTY(main.label1.caption)
	main.command2.Enabled = .t.
	main.label1.caption   = 'OK'
	main.label1.fontsize  = 70
endif
IF scaning && and
	IF secondtry = 2
		WAIT WINDOW 'done' TIMEOUT 1 NOWAIT	
		main.label1.backcolor = RGB(255, 0, 0)	
	ELSE
		WAIT WINDOW 'second try' TIMEOUT 1 NOWAIT
		*main.label1.backcolor = RGB(0, 0, 255)	
		RETURN .t.
	ENDIF 
ENDIF		 
RETURN .f.		

FUNCTION vihnotvih && ���������� �� ������� �� ��������
LPARAMETERS vihscandate, vihlstdrab, vihregrab, ifyes, ifnot, vihpriznak
DO CASE 
CASE LEN(RTRIM(vihregrab)) = 7 &&5\2, 4\3
	IF SUBSTR(vihregrab, DOW(vihscandate, 2), 1) = vihpriznak
		return ifyes
	ELSE
		return ifnot
	endif
OTHERWISE && 3\3, 2\3, 1\3
	vihregdays = LEN(RTRIM(vihregrab))
	DO case
	CASE vihlstdrab < vihscandate
		vihnumregday = MOD(vihscandate - vihlstdrab, vihregdays)
		vihregdays = 0
	CASE vihlstdrab => vihscandate
		vihnumregday = vihregdays - MOD(vihlstdrab - vihscandate, vihregdays)
	OTHERWISE
		
	ENDCASE 
	IF vihnumregday = vihregdays
		vihnumregday = 1
	ELSE
		vihnumregday = vihnumregday + 1
	ENDIF 
	IF SUBSTR(vihregrab, vihnumregday, 1) = vihpriznak
		return ifyes
	ELSE
		return ifnot
	endif
ENDCASE	